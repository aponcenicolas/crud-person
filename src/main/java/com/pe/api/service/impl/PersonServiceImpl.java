package com.pe.api.service.impl;

import com.pe.api.model.Person;
import com.pe.api.repository.PersonRepository;
import com.pe.api.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PersonServiceImpl implements PersonService {

  private final PersonRepository personRepository;

  @Override
  public List<Person> getPeople() {
    return personRepository.findAll();
  }

  @Override
  public Person getPerson(Long id) {
    return personRepository.findById(id).orElse(null);
  }

  @Override
  public Person savePerson(Person person) {
    return personRepository.save(person);
  }

  @Override
  public Person updatePerson(Long id, Person person) {
    Person editPerson = personRepository.findById(id).orElse(null);

    editPerson.setName(person.getName());
    editPerson.setPhone(person.getPhone());
    editPerson.setAge(person.getAge());
    editPerson.setAddress(person.getAddress());

    return personRepository.save(editPerson);
  }

  @Override
  public String deletePerson(Long id) {
    String message = "Error no se pudo eliminar a la persona";
    Person deletePerson = personRepository.findById(id).orElse(null);
    if (deletePerson.getId() != null) {
      personRepository.delete(deletePerson);
      message = "Persona eliminada.";
    }

    return message;
  }
}
