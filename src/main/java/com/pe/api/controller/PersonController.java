package com.pe.api.controller;

import com.pe.api.model.Person;
import com.pe.api.service.PersonService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/people")
@AllArgsConstructor
public class PersonController {

  private final PersonService personService;

  @GetMapping
  public List<Person> getPeople() {
    return personService.getPeople();
  }

  @GetMapping("/{id}")
  public Person getPerson(@PathVariable Long id) {
    return personService.getPerson(id);
  }

  @PostMapping()
  public Person savePerson(@RequestBody Person person) {
    return personService.savePerson(person);
  }

  @PutMapping("/{id}")
  public Person editPerson(@PathVariable Long id, @RequestBody Person person) {
    return personService.updatePerson(id, person);
  }

  @DeleteMapping("/{id}")
  public String deletePerson(@PathVariable Long id) {
    return personService.deletePerson(id);
  }
}
