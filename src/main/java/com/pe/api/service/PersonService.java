package com.pe.api.service;

import com.pe.api.model.Person;

import java.util.List;

public interface PersonService {
  List<Person> getPeople();

  Person getPerson(Long id);

  Person savePerson(Person person);

  Person updatePerson(Long id, Person person);

  String deletePerson(Long id);
}
